import ecdsa
import hashlib
import base58


def private_key_generator():
    private_key = ecdsa.SigningKey.generate(curve=ecdsa.SECP256k1)
    return private_key


def public_key_generator(private_key:ecdsa.keys.SigningKey):
    public_key = private_key.get_verifying_key()
    return public_key


def public_key_compressor(public_key:ecdsa.keys.VerifyingKey):
    x = public_key.to_string()[:32]
    y = public_key.to_string()[32:]
    # 홀수일때
    if y[31]&1:
        compressed_public_key = b'\x03'+x
    # 짝수일때
    else:
        compressed_public_key = b'\x02'+x
    return compressed_public_key


def bitcoin_address_generator(compressed_public_key):
    # SHA256 hash
    hash1 = hashlib.sha256(compressed_public_key).digest()

    # RIPEMD160 hash
    h = hashlib.new('ripemd160')
    h.update(hash1)
    hash2 = h.digest()

    # Add network byte
    hash2 = b'\x00'+hash2

    # SHA256 hash
    hash3 = hashlib.sha256(hash2).digest()

    # SHA256 hash
    hash4 = hashlib.sha256(hash3).digest()

    # 주소 생성
    hashsum = hash2+hash4[:4]
    address = base58.b58encode(hashsum)

    return address


def sign_with_private_key(private_key:ecdsa.keys.SigningKey, message):
    signature = private_key.sign(message)
    return signature


def verify_with_public_key(public_key:ecdsa.keys.VerifyingKey, signature, message):
    try:
        public_key.verify(signature, message)
        return True
    except ecdsa.BadSignatureError:
        return False


def main():
    # 개인키 생성
    private_key = private_key_generator()
    print('private key: 0x'+private_key.to_string().hex())
    print()

    # 공개키 생성
    public_key = public_key_generator(private_key)
    print('public key: 0x04'+public_key.to_string().hex())
    print('x: 0x'+public_key.to_string()[:32].hex())
    print('y: 0x'+public_key.to_string()[32:].hex())
    print()

    # 공개키 압축
    compressed_public_key = public_key_compressor(public_key)
    print('compressed public key: 0x'+compressed_public_key.hex())
    print()

    # 비트코인 주소 생성
    address = bitcoin_address_generator(compressed_public_key)
    print('address: '+address.decode('utf-8'))
    print()

    # 개인키로 전자 서명
    message = b'put your own message'
    signature = sign_with_private_key(private_key, message)

    # 공개키로 검증 - 옳은 검증
    verify_result = verify_with_public_key(public_key, signature, message)
    if verify_result:
        print("verified")
    else:
        print("not verified")
    print()

    # 공개키로 검증 - 잘못된 검증
    fake_message = b'fake message'
    verify_result = verify_with_public_key(public_key, signature, fake_message)
    if verify_result:
        print("verified")
    else:
        print("not verified")
    print()


main()
